@Library('global-shared-library@v1.0') _

properties([
    parameters([ 
        //PROJECT_PARAMETERS 部分 根据项目情况更新 
        separator(name: "PROJECT_PARAMETERS", sectionHeader: "Project Parameters"),
        string(name: 'PROJECT_NAME', defaultValue: 'vue-app', description: '项目名称') ,
        string(name: 'GIT_URL', defaultValue: 'git@git.xxxx.com.cn:devopsing/vuejs-docker.git', description: 'Git仓库URL') ,
        string(name: 'DOCKER_NAME', defaultValue: 'vuejs-docker', description: 'Docker镜像名称(必须小写)') ,

        //BUILD_PARAMETERS 部分，设置代码编译配置
        separator(name: "BUILD_PARAMETERS", sectionHeader: "Build Parameters"),
        gitParameter(name: 'BRANCH_TAG',
                     type: 'PT_BRANCH_TAG',
                     branchFilter: 'origin/(.*)',
                     defaultValue: 'master',
                     selectedValue: 'DEFAULT',
                     sortMode: 'DESCENDING_SMART',
                     description: 'Select your branch or tag.') ,
        
        choice(name: 'VUE_BUILD_ENV', choices: [ 'N/A', 'dev','stage', 'prod'], description: 'VUE编译时环境变量参数'),

        //DEPLOY_PARAMETERS 部分，只需添加部署环境IP即可，其他无需修改
        separator(name: "DEPLOY_PARAMETERS", sectionHeader: "Deploy Parameters"),
        choice(name: 'ACTION_TYPE', choices: [ 'CI&CD', 'CD'], description: '默认为"CI&&CD",即构建后直接部署当前产出;如果仅需要部署指定版本可选择"CD"'), 

        [$class: 'CascadeChoiceParameter', 
            choiceType: 'PT_SINGLE_SELECT', 
            description: '指定直接部署的版本，目前支持保留策略设定的版本个数', 
            filterLength: 1, 
            filterable: false, 
            name: 'DEPLOY_VERSION', 
            referencedParameters: 'ACTION_TYPE', 
            script: [
                $class: 'GroovyScript', 
                fallbackScript: [
                    classpath: [], 
                    sandbox: false, 
                    script: 
                        "return['Error!Could not get the builds']"
                ], 
                script: [
                    classpath: [], 
                    sandbox: false, 
                    script: '''
                        import jenkins.model.Jenkins
                        def jobName = "/Pipeline-Demo/vuejs-docker_v2.0"
                        if (ACTION_TYPE.equals("CD")) {
                            return Jenkins.getInstance().getItemByFullName("${jobName}").getBuilds().collect{it.getDisplayName()}.findAll{ it=~'^(([0-9]|([1-9]([0-9]*))).){2}([0-9]|([1-9]([0-9]*)))([_](([0-9A-Za-z]|([1-9A-Za-z]([0-9A-Za-z]*)))[-]*[.]*){0,}){2}$' }
                          } 
                        else {
                            return ["ACTION_TYPE为“CI&CD”时默认忽略该项，当ACTION_TYPE切换到“CD”显示该列表"]
                        }
                        '''
                ]
            ]
        ],
        choice(name: 'DEPLOY_SERVER', choices: [ 'N/A', '192.168.249.5', '192.168.249.25','192.168.249.18'], description: 'VUE部署Server') ,
        
        //MISC_PARAMETERS 部分，邮件发送规则：如果不显示声明收件人，则默认使用该项目gitlab group/projecct user email address
        separator(name: "MISC_PARAMETERS", sectionHeader: "Misc Parameters"),
        text(name: 'MAILRECIPIENTS', defaultValue: 'owenli@126.com;', description: 'Build通知收件人'),
        booleanParam( defaultValue: false, description: '是否发送邮件通知(默认为true)', name: 'DO_SENDEMAIL') 
    ])
])

pipeline {
    agent { label 'linux' }
    triggers{ //不需要gitlab push触发 jenkins job, 可删除该triiger; 
        gitlab(
            triggerOnPush: true, 
            triggerOnMergeRequest: true,
            branchFilterType: 'All',
            secretToken: "fe0b38d5xxxxb240ca257e94ee4974b")
    }
    options
    {   //一般情况，使用默认即可
        buildDiscarder(logRotator(numToKeepStr: '20'))
        disableConcurrentBuilds()  
        skipDefaultCheckout(true)     
        timestamps()
        gitLabConnection("gitlab")
    } 

    environment {       //一般情况，使用默认即可       
        HARBOR="harbor.xxxxx.com.cn"          
        HARBOR_ACCESS_KEY = credentials('harbor-userpwd-pair')     
        SERVER_ACCESS_KEY = credentials('deploy-userpwd-pair')      
        GITLAB_API_TOKEN = credentials('gitlab_api_token_secret')       
    }
    stages {
        stage('Prepare') {
            steps {  
                checkoutCode(ACTION_TYPE)
                generateVersion() 
                printEnv()
            }
        }
        stage('Build') {
            when {
                environment name: 'ACTION_TYPE', value: 'CI&CD'
            }
            steps {                
                buildDocker("vue")                           
            } 
            post
            {
                success
                {   pushDocker()                  
                    saveArtifacts("vue") 
                    cleanDocker("vue")
                }
            }         
        }
        stage('Deploy') {
            when {
              not { environment name: 'DEPLOY_SERVER', value: 'N/A' }
            }
            steps {
                runDocker("vue")    
            }
        }
    }
    post {    // 默认集成gitlab(需配置webhook), 不需要可删除 updateGitlabCommitStatus     
        failure{
            updateGitlabCommitStatus name: 'JenkinsPipeline', state: 'failed'
        }
        success{
            updateGitlabCommitStatus name: 'JenkinsPipeline', state: 'success'
        }
        always
        {
            sendEmail (currentBuild.result , currentBuild.displayName) 
        }
    }
}


