# Jenkins-Pipeline (Vue.js)

## 说明

该项目通过vue.js的容器化部署，展示了Jenkins Pipeline的设计。这是我接触jenkins之后，结合过往的项目中的devops实践经验，同时借助Jenkins 特有的能力，设计出来的流水线。

## 使用

这个demo中的pipeline, 开发人员只需要修改这个参数，其他配置jenkins统一配好就可以了（主要是“共享库” 和”凭证“）

``` text
    parameters { //根据项目情况更新 defaultValue/choices
        string(name: 'PROJECT_NAME', defaultValue: 'vue-app', description: '项目名称') 
        string(name: 'GIT_URL', defaultValue: 'git@gitee.com:devopsing/devops_practise.git', description: 'Git仓库URL') 
        string(name: 'DOCKER_NAME', defaultValue: 'vuejs-docker', description: 'Docker镜像名称') 
        gitParameter name: 'BRANCH_TAG',
                     type: 'PT_BRANCH_TAG',
                     branchFilter: 'origin/(.*)',
                     defaultValue: 'master',
                     selectedValue: 'DEFAULT',
                     sortMode: 'DESCENDING_SMART',
                     description: 'Select your branch or tag.'
        choice(name: 'VUE_BUILD_ENV', choices: [ 'N/A', 'dev','stage', 'prod'], description: 'VUE编译时环境变量参数')
        choice(name: 'DEPLOY_SERVER', choices: [ 'N/A', '192.168.31.104', '192.168.31.107','192.168.31.108'], description: '部署环境')
        text(name: 'EXTRA_PARAMETER', defaultValue:'', description: 'DockerRUN-额外参数')
        text(name: 'MAILRECIPIENTS', defaultValue: 'libin95188@126.com.cn', description: 'Build通知收件人')                
    }
```

## 功能展示

![pipeline](./Docs/pipeline-3.PNG)

![pipeline](./Docs/pipeline-4.PNG)

![pipeline](./Docs/pipeline-5.PNG)

## CI/CD 设计迭代过程

### Verion-0.1

Jenkins free style是过时的用法，不推荐使用！原因如下

- 复用性极差，整个流水线上下文，脚本的组合是割裂的

**“Everythin is Code”** 是我极力推荐的最佳实践。

### Version-1.0

Jenkin pipeline 是jenkins2.0的重要变化，确实很简单，对于个人使用，或者构建过程很简单是可以的。但是遇到复杂的构建过程，它就会显得”凌乱“，特别是jenkins太灵活了，没有devops实践的新手，就会写一大堆声明，充斥着hardcode. 当遇到新的项目，pipeline 又要重新写一次。

### Version-1.1 （tag v1.1）

根据实践经验，**没有标准化，就没有自动化和复用**，所以在这个版本里我开始把**变的**抽离出来，对每个pipeline到要使用的（比如，凭证）统一管理，用有意义的ID管理。

![凭证管理](./Docs/jenkins-凭证.PNG)

同时，定义出入参数、变量；同时结合项目本身，制定”专属“的pipeline.

可以看这个例子- <https://gitee.com/devopsing/vue-docker/tree/v1.1>

这个一定程度上解决了复用的问题，但是依然是jenkinsfile+shell组合，有时候不得不传递一大堆参数，相互依赖都不能少，可是开发人员不清楚啊。

### Version-2.0 (current version)

基于上面版本的问题，我开始考虑用”共享库“，这样就保留一个jenkinsfile, 使用方便。最终业务开发团队，只要拿到这个jenkinfile, 只需要更新 里面的git 地址，项目名称等业务有关的信息，置于CI/CD里面所有的细节，他们不需要关心。

![pipeline](./Docs/pipeline-1.PNG)

![pipeline](./Docs/pipeline-2.PNG)

另外只需要维护唯一的”工具库“，结合统一的”变量命名“，环境的迁移更改都很清晰，不用一个个去替换。

这种工具方法的组合，适用于流程规范还不是很清晰，技术没有完全定型，可以通过”颗粒度小“的方法进行自由组合，当然弊端就是共享库的维护很重要。

另外一种方式就是Jenkinsfile,几乎什么步骤都没有，只有一两句声明，这更适合已经很规范的技术栈，流程中所有的东西都统一了。

### Version-2.1

下一步准备考虑使用yaml配置文件方式去定义整个流水线，把更多的过程隐藏起来，开发人员只需要配置类似yaml的配置文件即可

## CI/CD 设计关键点

### 1. 分支模型

- 不同的分支模型决定了最终设计的流水线的方式

### 2. 版本号定义

- 它是产品整个流程的唯一标识，所有的变化都需要通过它来找到，需要清楚哪个版本包含了那些改动，哪个改动被包含进了哪个包，制品也需要包含版本属性

### 3. 构建配置

- 不同技术栈，配置方式也不同。比如vue.js, 它对应的API环境信息是需要编译时候就打进包里的； python 的编译是要在运行时候进行；spring cloud里面环境信息也是需要docker build 时候传递进去的，这些也决定了pipeline的设计模型

### 4. 多环境部署问题

- 多环境部署，一个角度是不同环境不同配置，不同的xml,json等；第二个是分支和不同环境的关系

- 多环境又牵涉到了发布频率和 发布的审批，发布的策略等

